FROM i386/debian
ADD tn5250_0.16.5-5woody1_i386.deb /
ADD libssl0.9.6_0.9.6c-0.potato.6_i386.deb /
RUN dpkg -i libssl0.9.6_0.9.6c-0.potato.6_i386.deb && dpkg -i tn5250_0.16.5-5woody1_i386.deb || true
RUN apt-get --fix-missing update
RUN apt-get install -f -y
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y locales
ENV LANGUAGE="de_DE:de"
ENV LANG="de_DE.iso88591"
RUN sed -i -e 's/# de_DE ISO-8859-1/de_DE ISO-8859-1/' /etc/locale.gen
RUN locale-gen

ENTRYPOINT ["tn5250"]
CMD [""]
